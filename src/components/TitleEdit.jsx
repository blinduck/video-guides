import React from 'react';

const TitleEdit = ({tempTitle, titleChanged, saveTitleChange, changeTitleEditMode}) => {
   const onCancel = () => {
       changeTitleEditMode(false)
   }
   return (
       <div className="uk-flex uk-flex-row uk-margin-top uk-flex-middle">
           <input className="uk-input uk-flex-1" placeholder="title..."
                  type="text" value={tempTitle} onChange={titleChanged}
           />
           <button className="uk-button uk-button-text uk-margin-left"
                   onClick={saveTitleChange}>Save
           </button>
           <button className="uk-button uk-button-text uk-margin-left"
                   onClick={onCancel}>Cancel
           </button>
       </div>
   )
}

export default TitleEdit;
