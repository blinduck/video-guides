import React from 'react';

const Title = ({title, canEdit, startEdit}) => {
    return (
        <div className="uk-flex uk-flex-row uk-flex-middle uk-margin-medium-top">
            <h3 className="uk-flex-1 uk-margin-remove-bottom">{title}</h3>
            {canEdit ?
                <button className="uk-button uk-button-text"
                        onClick={startEdit}>Edit</button> : null
            }
        </div>
        )
}

export default Title
