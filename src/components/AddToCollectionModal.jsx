import React, {useState, useEffect, useRef} from 'react';
import {DataAdapter} from "../DataAdapter";


const AddToCollectionModal = ({noteUID}) => {
    const [collections, setCollections] = useState([]);
    const [isFetching , setIsFetching] = useState(false);
    const [initialFetchCompleted, setInitialFetchCompleted] = useState(false)
    const modalRef = useRef(null);


    const onShow = () => {
        if (initialFetchCompleted) return;
        setIsFetching(true);
        DataAdapter.get_collections_list().then(resp => {
            setCollections(resp.data.results);
            setIsFetching(false);
            setInitialFetchCompleted(true);
        })

    }

    useEffect(() => {
        if (modalRef.current && !initialFetchCompleted) {
            modalRef.current.addEventListener("show", onShow)
        }
        return () => {
            if (modalRef.current) {
                modalRef.current.removeEventListener("show", onShow)
            }
        }
    });


    return (
        <div id="add-collection-modal" data-uk-modal ref={modalRef}>
            <div className="uk-modal-dialog uk-modal-body">
                <h2 className="uk-modal-title"></h2>
                {isFetching && <div>Fetching...</div>}
                <RenderCollections collections={collections} noteUID={noteUID}/>
                <p className="uk-text-right">
                    <button className="uk-button uk-button-default uk-modal-close" type="button">Close</button>
                </p>
            </div>
        </div>
    )
}


const RenderCollections = ({collections, noteUID}) => {
    const [localCollections, setLocalCollections] = useState(collections);
    
    useEffect(() => {
        console.log('use effect called');
        setLocalCollections(collections)
    }, [collections])

    console.log('collections', collections)
    const addToCollection = (c) => {
        DataAdapter.add_to_collection(noteUID, c.uid).then(resp => {
            const newLocalCollections = localCollections.map(c => {
                if (c.uid === noteUID) c.added = true;
                return c
            })
            setLocalCollections(newLocalCollections);
        })
    }

    if (!localCollections || localCollections.length === 0 ) {
        return null;
    }

    const noteAlreadyAdded = (c, noteUID) => {
        return c.added ||  c.notes.findIndex(cNoteUID => cNoteUID === noteUID) >= 0
    }

    return <div>
        <h4 className="uk-margin-large-bottom">Select Collection</h4>
        <dl className="uk-description-list uk-description-list-divider">
            {localCollections.map(c => {
                return <dt className="uk-flex uk-flex-between">
                    <div>{c.name}</div>
                        {noteAlreadyAdded(c, noteUID) ?
                            <button disabled className="uk-button uk-button-default">Added</button> :
                            <button onClick={addToCollection.bind(this, c)} className="uk-button uk-button-primary" type="button">Add</button>
                        }
                </dt>
            })}
        </dl>
    </div>
}

export default AddToCollectionModal