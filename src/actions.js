
export const LOGIN_STATE = "LOGIN_STATE";
export const EDIT_STEP = "EDIT_STEP";
export const EDIT_TITLE = "EDIT_TITLE";
export const SET_CURRENT_LOOP = "SET_CURRENT_LOOP";


export function login_state(new_val) {
    return {
        type: LOGIN_STATE,
        new_val: new_val
    }
}

export function editStepChange(id) {
    return {
        type: EDIT_STEP,
        id: id
    }
}

export function changeTitleEditMode(new_val) {
    return {
        type: EDIT_TITLE,
        new_val: new_val
    }
}

// expects {startSeconds, endSeconds, startTime, endTime}
export function setCurrentLoop(currentLoop) {
    return {
        type: SET_CURRENT_LOOP,
        currentLoop,
    }
}


export const initalState = {
    logged_in: false,
    editable_step: null,
    titleEditMode: false,
    currentLoop: null,
}




export function tubenotesApp(state=initalState, action) {
    switch(action.type) {
        case LOGIN_STATE:
            return Object.assign({}, state, { logged_in: action.new_val });
        case EDIT_STEP:
            return Object.assign({}, state, {editable_step: action.id});
        case EDIT_TITLE:
            return Object.assign({}, state, {titleEditMode: action.new_val});
        case SET_CURRENT_LOOP:
            return Object.assign({}, state, {currentLoop: action.currentLoop})
        default:
            return state
    }
}

