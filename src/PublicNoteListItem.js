import {Link} from "react-router-dom";
import React from "react";
import moment from "moment";

const PublicNoteListItem = (note) => {
    let link = `notes/${note.uid}`;
    return (<div className="uk-card-default uk-card-small uk-card-body" key={note.uid}>
        <Link className="uk-width-expand" to={link}>
            <img className="uk-width-expand" src={`https://img.youtube.com/vi/${note.video_code}/hqdefault.jpg`} alt=""/>
        </Link>
        <div className="">
            <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-top"><Link className="uk-link uk-link-text" to={link}>{note.title}</Link></h3>
                <div className="uk-flex">
                    <span className="uk-flex-1">{ moment(note.created_at).format("h:mm A, Do MMM")}
                    </span>
                </div>
                {/*
                <p>{note.step_count} steps</p>
                */}
            </div>
        </div>
    </div>)
}


export {PublicNoteListItem}
