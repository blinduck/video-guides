import React from 'react';


class FeedbackView extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return <div className="uk-container">
            <p>I hope you've enjoyed using TubeNotes.</p>
            <p>
                Please send me feedback, ideas or anything else at hellotubenotes@gmail.com.
            </p>
        </div>
    }
}

export {FeedbackView}

