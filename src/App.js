import React, {Fragment} from 'react';
import './App.css';
import {Link, Route} from "react-router-dom";
import {DataAdapter, Store} from './DataAdapter'
import {NotesListView} from "./NotesListView";
import NotesDetailView from "./NoteDetailView";
import {SignUpView} from "./SignUpView";
import {createStore} from 'redux';
import {initalState, login_state, tubenotesApp} from "./actions";
import {connect, Provider} from "react-redux";
import {LoginView} from "./LoginView";
import {NoteCreate} from "./NoteCreate";
import {Home} from "./Home";
import {FeedbackView} from "./Feedback";
import ReactGA from 'react-ga'
import LandingPage from "./pages/LandingPageView";
import CollectionsListView from "./pages/CollectionsListView";
import CollectionsDetailView from "./pages/CollectionsDetailView";
import ExampleNoteView from './pages/ExampleNoteView';


let token = Store.getToken();
let preloadedState = token ?
    Object.assign({}, initalState, {logged_in: true})
    : {};

export const ReduxStore = createStore(
    tubenotesApp,
    preloadedState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

const mapStateToProps = state => {
    return {
        login_state: state.logged_in
    }
};

const mapDispatchToProps =  dispatch => {
    return {
        logout: () => {
            Store.logOut();
            dispatch(login_state(false))


        }
    }
}

class Navbar extends React.Component {

    constructor(props) {
       super(props);
       this.props = props;
    }

    logoutClicked = (evt) => {
        this.props.logout();
    }

    render() {
        return <nav className="uk-navbar-container navbar-block" uk-navbar="true">
            <div className="uk-navbar-left">
                <ul className="uk-navbar-nav">
                    <li>
                        <Link to="/">
                            <img id="navbar-icon" src="images/tubenotes-logos_black_cropped.png" alt=""/>
                        </Link>
                    </li>
                    {this.props.login_state &&
                        <Fragment>
                            <li>
                                <Link to="/notes">Notes</Link>
                            </li>
                            {/*
                            <li>
                            <Link to="/collections">Collections</Link>
                            </li>
                            */}
                            <li>
                                <Link to="/create">Create</Link>
                            </li>
                        </Fragment>
                }
                </ul>
            </div>
            <div className="uk-navbar-right">
                {this.props.login_state ?
                    <ul className="uk-navbar-nav">
                        <li><Link to="/feedback">Feedback</Link></li>
                        <li>
                            <a className="uk-button  uk-button-text uk-margin-right" onClick={this.logoutClicked} href="/">Logout</a>
                        </li>
                    </ul> :
                    <ul className="uk-navbar-nav">
                        <li><Link to="/feedback">Feedback</Link></li>
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                        <li>
                            <Link to="/signup">Sign Up</Link>
                        </li>

                    </ul>
                }
            </div>
        </nav>
    }
};


const VisibleNavBar = connect(mapStateToProps, mapDispatchToProps)(Navbar);

class App extends React.Component {

    componentDidMount() {
        console.log('Initializing GA');
        ReactGA.initialize('UA-159853061-1');
        let user =  Store.getUser();
        console.log('User', user);
        if (user) {
            ReactGA.set({userId: user.id});
        }
    }

    render() {
        return (
            <Provider store={ReduxStore}>
            <div>
                <VisibleNavBar/>
                <div className="uk-margin">
                    <Route exact path="/" component={LandingPage}></Route>
                    <Route exact path="/feedback" component={FeedbackView}></Route>
                    <Route exact path="/notes" component={NotesListView}></Route>
                    <Route path="/landing" component={LandingPage}></Route>
                    <Route exact path="/collections" component={CollectionsListView}></Route>
                    <Route path="/collections/:uid" component={CollectionsDetailView}></Route>
                    <Route path="/notes/:uid" component={NotesDetailView}></Route>
                    <Route path="/create" component={NoteCreate}></Route>
                    <Route path="/signup" component={SignUpView}></Route>
                    <Route path="/login" component={LoginView}></Route>
                    <Route path="/example" component={ExampleNoteView}></Route>
                </div>
            </div>
            </Provider>
        )
    }
}

export default App;
