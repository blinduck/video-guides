import {DataAdapter} from "./DataAdapter";
import React from 'react';


class NoteCreate extends React.Component {

    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            video_code: '',
            is_private: false,
            title: '',
        };
        this.videoCodeChanged = this.videoCodeChanged.bind(this);
        this.isPrivateChanged = this.isPrivateChanged.bind(this);
        this.next_step = this.next_step.bind(this);
        this.validVideoId = this.validVideoId.bind(this);
    }

    videoCodeChanged(evt) {
        this.setState({video_code: evt.target.value})
    }

    isPrivateChanged(evt) {
        this.setState({is_private: evt.target.checked})
    }
    titleChanged = (evt) => {
        this.setState({title: evt.target.value})
    }

    next_step(evt) {
        const {video_code, is_private, title} = this.state;
        evt.preventDefault();

        this.validVideoId().then(valid => {
            if (!valid) {
                return alert("Invalid Youtube Video Id");
            }
            DataAdapter.create_note(video_code, is_private, title).then(resp => {
                this.props.history.push({
                    pathname: `notes/${resp.data.uid}`,
                })
            }).catch(error => {
                alert("Error creating new note.. Please try again.")
            })

        });
    }

    validVideoId() {
        return new Promise((resolve, reject) => {
            var img = new Image();
            img.src = "http://img.youtube.com/vi/" + this.state.video_code + "/mqdefault.jpg";
            img.onload = function () {
                console.log('Width', this.width)
                resolve(this.width !== 120);
            }
        });
    }


    render() {
        let {is_private, video_code, title} = this.state;

        return (
            <div className="uk-container">
                <div className="uk-card uk-card-default">
                    <form onSubmit={this.next_step}>
                        <div className="uk-card-header">Create a new note</div>
                        <div className="uk-card-body">
                            <div className="uk-margin-medium-bottom">
                                <label>Title: </label>
                                <input className="uk-input"
                                       type="text" value={title} onChange={this.titleChanged}
                                       required
                                />
                            </div>
                            <div>
                                <label>Video Code: </label>
                                <input className="uk-input"
                                       type="text" value={video_code} onChange={this.videoCodeChanged}
                                       required minLength="11" maxLength="11"
                                />
                                <p>For example: https://www.youtube.com/watch?v=<span
                                    className="uk-text-bold uk-text-primary">YS4e4q9oBaU</span></p>
                            </div>
                            <div className="uk-margin">
                                <label>Private:</label>
                                <input className="uk-checkbox uk-margin-small-left" type="checkbox" value={is_private}
                                       onChange={this.isPrivateChanged}/>
                            </div>
                        </div>
                        <div className="uk-card-footer">
                            <button type="submit" className="uk-button uk-button-primary">Next</button>
                        </div>
                    </form>
                </div>

            </div>
        )
    }
}

export {NoteCreate}
