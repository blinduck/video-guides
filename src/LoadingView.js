import React from "react";

const LoadingView = () => {
    return (
        <div className="uk-flex uk-flex-center">
            <div data-uk-spinner={''}></div>
        </div>)
}


export default LoadingView;
