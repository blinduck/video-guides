import {Link} from "react-router-dom";
import React from "react";
import moment from "moment";

const NoteListItem = (note, onDelete) => {
    let v_private = note.is_private ? <span className="uk-label uk-label-warning">Private</span> : <span className="uk-label">Public</span>;
    return (<div className="uk-card-default uk-card-small uk-card-body" key={note.uid}>
        <Link className="uk-width-expand" to={`notes/${note.uid}`}>
            <img className="uk-width-expand" src={`https://img.youtube.com/vi/${note.video_code}/hqdefault.jpg`} alt=""/>
        </Link>
        <div className="">
            <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-top"><Link className="uk-link uk-link-text" to={`notes/${note.uid}`}>{note.title}</Link></h3>
                <div className="uk-flex">
                    <span className="uk-flex-1">{ moment(note.created_at).format("h:mm A, Do MMM")}
                    </span>
                    {v_private}
                </div>
                <div className="uk-flex uk-margin-small-top">
                    <p className="uk-flex-1">{note.step_count} steps</p>
                    <button onClick={onDelete} className="uk-button uk-button-text">
                        <span uk-icon="icon: trash"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>)
}


export {NoteListItem}
