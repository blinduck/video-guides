import axios from 'axios';
import store from 'store';

let baseURL;

if (process.env.NODE_ENV  === "production") {
    baseURL = "https://tubenotes.xyz/api"
} else {
    baseURL = "http://localhost:8000/api";
}

console.log('Node env', process.env.NODE_ENV);

let urls = {
    signup: `${baseURL}/signup`,
    login: `${baseURL}/login`,
    my_notes: `${baseURL}/notes`,
    note_detail: (uid) => `${baseURL}/notes/${uid}`,
    note_update: (uid) => `${baseURL}/notes/edit/${uid}`,
    note_delete: (uid) => `${baseURL}/notes/edit/${uid}`,
    public_notes: `${baseURL}/public/notes`,
    collections_list: `${baseURL}/collections`,
    collections_detail: (uid) => `${baseURL}/collections/${uid}`,
    add_to_collection: (uid) => `${baseURL}/collections/${uid}/add`,
}


let axFetch = (method, url, data=null, auth=true) => {
    let options = {
        method: method,
        url: url,
        data: data,
        headers: {
            'content-type': 'application/json',
        }
    }
    if (auth) {
        options['headers']['Authorization'] = `Token ${Store.getToken()}`
    }
    return axios(options)
};

let axGet = (url, params, auth=true) => {
    let options = {
        method: "GET",
        url: url,
        params: params,
        headers: {
            'content-type': 'application/json',
        }
    }
    if (auth) {
        options['headers']['Authorization'] = `Token ${Store.getToken()}`
    }
    return axios(options)

}

class DataAdapter {

    getBaseAddress() {
        let nd = process.env.NODE_DEVELOPMENT;
        console.log('nd', nd);
        if (nd) {}
    }

    static signup(data) {
        return axFetch("POST", urls.signup, data, false)
    }

    static login(data) {
        return axFetch("POST", urls.login, data, false)
    }
    static my_notes(page_no) {
        return axGet(urls.my_notes, {page: page_no}, true)
    }
    static public_notes(page_no=1) {
        return axGet(urls.public_notes, {page: page_no}, false)
    }

    static note_detail(uid) {
        return axFetch("GET", urls.note_detail(uid)).then(resp => {
            console.log("note detail resp", resp)
            return new Note(resp.data)
        })
    }

    static public_note_detail(uid) {
        return axFetch("GET", urls.public_note_detail(uid)).then(resp => {
            return new Note(resp.data)
        })
    }
    static create_note(video_code, is_private, title){
        return axFetch("POST", urls.my_notes, {video_code, is_private, title})
    }

    static get_collections_list(page_no=1) {
        return axFetch('GET', urls.collections_list, true)
    }

    static get_collections_detail(uid) {
        return axFetch('GET', urls.collections_detail(uid), true)
    }

    static add_to_collection(note_uid, collection_uid) {
        return axFetch('POST', urls.add_to_collection(collection_uid), {note_uid})
    }
}


class Store {
    static storeToken = (token)=> {
        store.set('token', token)
        //docCookies.setItem('token', token)
    };
    static getToken = () => {
        //const token = docCookies.getItem('token');
        //return token;
        return store.get('token')
    };

    static setUser = (data) => {
        //docCookies.setItem('user', JSON.stringify(data))
        store.set('user', data)
    };

    static getUser = () => {
        //return JSON.parse(docCookies.getItem('user'))
        return store.get('user')
    };
    static logOut = ()=> {
        store.clearAll();
    };

    static isLoggedIn = ()=> {
        //if (docCookies.getItem('token')) {
        if (store.get('token')) {
            return true
        }
        return false
    };

}

class Collection {
    constructor(params) {
        Object.assign(this, params)
    }
}


class Note {
    constructor(params) {
        this.uid = params.uid;
        this.video_code = params.video_code;
        this.title = params.title || "";
        this.steps = params.steps ?
            params.steps.map(values => new Step(values)) : [];
        this.owner_id = params.owner_id;
    }

    update(){
        return axFetch("PUT", urls.note_update(this.uid), this).then(resp => {
            console.log("resp data", resp.data);
            return new Note(resp.data);
        })
    }
    delete() {
        return axFetch("DELETE", urls.note_delete(this.uid))
    }
}


class Step {
    constructor(values) {
        this.id = values.id;
        this.title = values.title || "";
        this.startTime = values.startTime || "";
        this.endTime = values.endTime || "";
        this.text = values.text || "<div></div>";
        this.newStep =  values.newStep === true;
        this.isLoop = values.isLoop || false;
        this.playbackRate = values.playbackRate || 1;
    }

}

export {DataAdapter, Step, Note, Collection, Store, urls}