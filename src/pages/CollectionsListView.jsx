import React from 'react';
import {DataAdapter} from "../DataAdapter";
import LoadingView from "../LoadingView";
import {Link} from "react-router-dom";
import {NoteListItem} from "../NoteListItem";
import moment from "moment";

class CollectionsListView extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            collections: []
        }
    }

    componentDidMount() {
        DataAdapter.get_collections_list().then(resp => {
            this.setState({
                loading: false,
                collections: resp.data.results,
            })
        })
    }

    render () {
        const {loading, collections} = this.state
        if (loading) {
            return <LoadingView/>
        }

        return (<div className="uk-padding-large">
            <div className="uk-grid
            uk-child-width-1-1@s
            uk-child-width-1-2@m
            uk-child-width-1-3@l
            uk-grid-match" data-uk-grid>
                {collections.map(c => {
                    return <div className="uk-card uk-card-default uk-card-small">
                        <div className="uk-card-body">
                            <Link to={`/collections/${c.uid}`}>{c.name}</Link>
                            <p>
                                { `Last updated: ${moment(c.updated_at).format("h:mm A, Do MMM")}`}
                            </p>
                        </div>
                    </div>
                })}
            </div>
        </div>)
    }
}

export default CollectionsListView
