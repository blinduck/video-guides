import React from 'react';
import {Link} from "react-router-dom";

const LandingPage = () => {
    return <div className="uk-container">
        <div className="uk-padding">
            <div className="uk-text-center" style={{fontSize: '30px', color: "black"}}>
                Tubenotes lets you annotate videos, <br/>and loop specific sections of interest.
            </div>
            <div className="uk-flex uk-flex-middle uk-flex-column">
                <img src="images/explainer.png" id="explainer" alt=""/>
                <p style={{fontSize: '30px'}} className="uk-flex uk-flex-center"><Link to="/example">See An Example</Link></p>
            </div>
        </div>
    </div>
}

export default LandingPage;