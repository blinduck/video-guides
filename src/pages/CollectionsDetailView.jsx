
import React from 'react';
import {DataAdapter} from "../DataAdapter";
import LoadingView from "../LoadingView";
import {Link} from "react-router-dom";
import {Collection} from "../DataAdapter";
import {stringify} from "query-string";

class CollectionsDetailView extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            collection: null,
        }
    }

    componentDidMount() {
        DataAdapter.get_collections_detail(this.props.match.params.uid).then(resp => {
            const collection = new Collection(resp.data)
            this.setState({
                loading: false,
                collection,
            })
        })
    }

    render () {
        const {loading, collection} = this.state;
        if (loading) {
            return <LoadingView/>
        }

        return (
            <div className="uk-padding-large">

                <div>Test</div>
                <div>{JSON.stringify(collection)}</div>
            </div>

        )


    }
}

export default CollectionsDetailView;
