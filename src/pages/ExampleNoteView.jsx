import React from 'react';
import NoteDetailView from "../NoteDetailView";

const noteData = {
    "owner_id":3,
    "uid":"b780c3ba-4034-4b4d-9700-a0617d8a07e6",
    "video_code":"QeDTF0aAqOQ",
    "title":"Chris Isaak - Wicked Game Guitar Tutorial",
    "original_title":"",
    "steps":[
        {"id":1,"text":"<p>3 Chords</p>\n<p>Bmin, A, E, E</p>\n<p><br></p>",
            "title":"Rhythm guitar chords","isLoop":true,"endTime":"7:10","startTime":"5:37","playbackRate":1},
        {"id":2,"text":"<p><br></p>",
            "title":"Rhythm Guitar Strumming Pattern","isLoop":false,"endTime":"","newStep":false,"startTime":"7:15","playbackRate":1},
        {"id":3,"text":"<p>Open B, Bend down half step</p>\n<p>7th Fret, bend back to return to pitch.</p>\n<p>Slide 7th Fret away, followed by open B</p>\n<p><br></p>\n<p>2nd Line</p>\n<p>Open B, Bend down half step</p>\n<p>7th Fret, bend back to return to pitch.</p>\n<p>5F, 3F, 2F, Open B with trem</p>\n<p><br></p>\n<p><br></p>",
            "title":"Lead Guitar 1","isLoop":true,"endTime":"11:12","newStep":false,"startTime":"9:54","playbackRate":1},
        ],
    "created_at":"2021-05-30T09:19:37.996693Z",
    "updated_at":"2021-05-30T11:18:40.487996Z","step_count":4,"is_private":false}

const ExampleNoteView = (props) => {
    console.log('higher', props);
    return <NoteDetailView note={noteData} history={props.history}/>
}

export default ExampleNoteView;
