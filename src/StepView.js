import React from 'react';
import {DataAdapter} from "./DataAdapter";
import {Link} from "react-router-dom";
import UIkit from "uikit";
import {connect} from "react-redux"
import {stringTimeToSeconds} from "./utils/timeUtils";



class StepView extends React.Component {

    loopClicked = () => {
        const {setCurrentLoop, step} = this.props;
        // emits action
        setCurrentLoop({
            startSeconds: stringTimeToSeconds(step.startTime),
            endSeconds: stringTimeToSeconds(step.endTime),
            startTime: step.startTime,
            endTime: step.endTime,
            playbackRate: step.playbackRate,
        })

    }

    skipToClicked = () => {
        const {step, skipTo} = this.props;
        skipTo(step);
    }


    render() {
        const { step, isOwner, startEdit, deleteStep} = this.props;

        return (
            <div className="uk-card uk-card-default uk-card-small">
                <div className="uk-card-header">
                    <div className="uk-flex uk-flex-middle">
                        <div className="uk-flex-1">{step.title}</div>
                        {isOwner ?
                            <div>
                                <button disabled={this.props.editable_step !== null}
                                        className="uk-button uk-button-default uk-button-danger"
                                        onClick={deleteStep}
                                        style={{padding: "0 10px"}}
                                        uk-icon="icon: trash"/>
                                <button disabled={this.props.editable_step !== null}
                                        onClick={startEdit} className="uk-button uk-button-default uk-button-primary"
                                        style={{padding: "0 10px"}}
                                        uk-icon="icon: file-edit"/>
                            </div> : null
                        }
                    </div>
                    <div className="uk-margin-small-top">
                        {step.startTime ?
                            <button className="uk-button uk-button-primary uk-button-small"
                                    onClick={this.skipToClicked}>
                                {`Seek to ${step.startTime}`}
                            </button> : null
                        }
                        {step.startTime && step.endTime ?
                            <button className="uk button uk-button-primary uk-button-small uk-margin-left"
                                    onClick={this.loopClicked}>
                                {`Loop: ${step.startTime} - ${step.endTime}`}
                            </button> : null
                        }
                    </div>
                </div>
                {step.text ?
                    <div className="uk-card-body step-text" dangerouslySetInnerHTML={{__html: step.text}}>
                    </div> : null
                }
            </div>
        )
    }
}


const mapStateToProps  = state => {
    return {
        editable_step: state.editable_step
    }
}

const mapDispatchToProps = dispatch => {
   return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(StepView);
