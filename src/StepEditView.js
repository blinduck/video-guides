import React from 'react';
import {DataAdapter, Step} from "./DataAdapter";
import {Link} from "react-router-dom";
import UIkit from "uikit";
import {cloneDeep} from 'lodash'
import {RichTextEditor} from "./RichTextEditor";
import {Editor, EditorState, RichUtils, getDefaultKeyBinding, ContentState} from "draft-js"
import {stateFromHTML} from 'draft-js-import-html';
import {stateToHTML} from 'draft-js-export-html';
import {connect} from "react-redux";
import { isStepTimeValid} from "./utils/stepUtils";


class StepEditView extends React.Component {
    constructor(props) {
        super(props);
        this.originalStep = props.step;
        let stepCopy = cloneDeep(props.step);
        this.state = {
            step: stepCopy,
        };
        this.editorState = EditorState.createWithContent(
            stateFromHTML(stepCopy.text)
        );
    }



    save = (evt) => {
        const { step } = this.state;
        if (!isStepTimeValid(step)) {
            alert("Invalid time format");
            return;
        }


        if (!step.isLoop) {
            delete step.endTime
        }
        step.text = stateToHTML(this.editorState.getCurrentContent());
        this.props.finishEdit(this.originalStep, step);
    }

    editorStateUpdated = (es) => {
        this.editorState = es;
    }


    inputChange = (evt) => {
        const { step } = this.state;
        this.setState({
            step: {
                ...step,
                [evt.target.name]: evt.target.value
            }
        })
    }

    setPlaybackRate = (evt) => {
        const { step } = this.state;
        this.setState({
            step: {
                ...step,
                playbackRate: parseFloat(evt.target.value)
            }
        })
    }

    setLoop = (evt) => {
        const { step } = this.state;
        this.setState({
            step: {
                ...step,
                isLoop: evt.target.checked,
            }
        })
    }

    render(){
        const { step } = this.state
        return (
            <div className="uk-card uk-card-default uk-card-small">
                <div className="uk-card-header">
                    <div>
                        <input type="text" name="title" className={"uk-input"}
                                value={step.title} onChange={this.inputChange} placeholder="Title..."/>
                    </div>
                    {step.isLoop ?
                        <div className="uk-margin">
                            <div className="uk-margin">
                                <span>Loop from(hh:mm:ss or mm:ss or ss): </span>
                                <input className={"uk-input uk-form-width-small uk-margin-small-left"}
                                       type="text" name="startTime" value={step.startTime} onChange={this.inputChange} placeholder="1:00"/>
                                <span>to </span>
                                <input className={"uk-input uk-form-width-small uk-margin-small-left"}
                                       type="text" name="endTime" value={step.endTime} onChange={this.inputChange} placeholder="1:00"/>
                            </div>
                        </div> :
                        <div className={"uk-flex uk-margin uk-flex-middle"}>
                            <span>Seek To (hh:mm:ss or mm:ss or ss): </span>
                            <input className={"uk-input uk-form-width-small uk-margin-small-left"}
                                   type="text" name="startTime" value={step.startTime} onChange={this.inputChange} placeholder="1:00"/>
                        </div>
                    }
                    <div>
                        <input
                            className="uk-checkbox" type="checkbox" name="isLoop" checked={step.isLoop} onChange={this.setLoop}/>
                            <span className="uk-margin-small-left">Create a loop</span>
                    </div>
                    <div className="uk-flex uk-margin uk-flex-middle">
                        <span>Playback speed:</span>
                        <input className="uk-input uk-form-width-small uk-margin-small-left" type="number"
                               value={step.playbackRate} name="playbackRate"
                               onChange={this.setPlaybackRate} min="0" step="0.01"/>
                    </div>
                </div>
                <div className="uk-card-body">
                    <RichTextEditor editorState={this.editorState} stateUpdated={this.editorStateUpdated} ></RichTextEditor>
                </div>
                <div className="uk-card-footer">
                    <div className="uk-flex uk-flex-right">
                        <button className={"uk-button uk-button-primary uk-margin-right"} onClick={this.save}>Save</button>
                        <button className={"uk-button uk-button-danger"} onClick={this.props.cancelEdit}>Cancel</button>
                    </div>

                </div>
            </div>
        )
    }

}


export default StepEditView;
