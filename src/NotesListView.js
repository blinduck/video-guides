import React from 'react';
import {DataAdapter, Note} from "./DataAdapter";
import {Link} from "react-router-dom";
import {NoteListItem} from "./NoteListItem";
import queryString from "query-string";
import LoadingView from "./LoadingView";

class NotesListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            page: this.getPage()
        };
        this.getPage = this.getPage.bind(this);
        this.updateData = this.updateData.bind(this);
        this.deleteNote = this.deleteNote.bind(this)
    }


    updateData(page) {
        DataAdapter.my_notes(page).then((resp) => {
            this.setState({
                loading: false,
                notes: resp.data.results,
                page: this.getPage(),
                nextPage: resp.data.next ? page + 1 : null,
                prevPage: resp.data.previous ? page - 1 : null
            })
        }).catch(error => {
            alert("Unable to get your notes");
        })

    }

    deleteNote(note){
        let proceed = window.confirm("Delete this note?")
        if (!proceed) return
        let n = new Note(note);
        n.delete().then(() => {
            this.updateData(this.getPage());
        });
    }

    getPage(){
        let params = queryString.parse(this.props.location.search);
        return parseInt(params.page) || 1
    }

    componentDidMount() {
        this.updateData(this.getPage())
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let p = this.getPage();
        if (p !== prevState.page) {
            this.updateData(p)
        }
    }

    render() {
        if (this.state.loading) {
            return LoadingView()
        }


        let {notes, prevPage, nextPage} = this.state;

        if (notes.length === 0) {
           return NoNotes()
        }

        return (
            <div className="uk-container">
                <div className="uk-padding-large">
                    <div className="uk-grid uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-4@l uk-grid-match" data-uk-grid>
                        { notes.map(note => NoteListItem(note, this.deleteNote.bind(this, note))) }
                    </div>
                    <div>
                        <ul className="uk-pagination uk-margin-top">
                            { prevPage ?
                                <li>
                                    <Link to={`/notes?page=${prevPage}`}>
                                        <span className="uk-margin-small-right" data-uk-pagination-previous></span> Previous
                                    </Link>
                                </li> : null }
                            {
                                nextPage ?
                                    <li className="uk-margin-auto-left">
                                        <Link to={`/notes?page=${nextPage}`}>Next
                                            <span className="uk-margin-small-left" data-uk-pagination-next></span>
                                        </Link>
                                    </li> : null
                            }
                        </ul>
                    </div>
                </div>

            </div>
    )
    }
}

const NoNotes = () => {
    return (<div className="uk-padding-large">
        <h4>No notes yet</h4>
        <Link to="/create">Create</Link>

    </div>)
}

export {NotesListView}
