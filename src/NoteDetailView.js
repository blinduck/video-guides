import React from 'react';
import {DataAdapter, Step, Store} from "./DataAdapter";
import StepView from "./StepView";
import StepEditView from "./StepEditView";
import _ from 'lodash';
import {editStepChange, changeTitleEditMode, setCurrentLoop} from "./actions";
import {connect} from "react-redux"
import {ReduxStore} from "./App";
import YouTubePlayer from 'react-player/lib/players/YouTube'
import TitleEdit from "./components/TitleEdit";
import Title from './components/Title';
import {stringTimeToSeconds} from "./utils/timeUtils";
import AddToCollectionModal from "./components/AddToCollectionModal";


class NotesDetailView extends React.Component {
    constructor(props) {
        super(props);
        this.uid = _.get(this.props, 'match.params.uid');

        this.state = {
            loading: true,
            playbackRate: 1,
            playbackRateInput: 1,

        };
        this.props = props;
        this.user = Store.getUser() || {};
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (_.isEqual(prevProps.currentLoop, this.props.currentLoop)) return;
        const playbackRate = _.get(this.props, 'currentLoop.playbackRate');
        if (!playbackRate || isNaN(playbackRate)) {return}
        this.setState({
            playbackRate,
            playbackRateInput: playbackRate
        })
    }


    componentDidMount() {
        this.props.setCurrentLoop(null);
        console.log('this.props', this.props);

        if (this.props.note) {
            // Example page
            this.setState({
                loading:false,
                isOwner: false,
                note: this.props.note
            });
            return;
        }

        DataAdapter.note_detail(this.uid).then(note => {
            this.setState({
                loading: false,
                isOwner: note.owner_id === this.user.id,
                note: note,
            })
        }).catch(error => {
            alert("Could not load that note");
            this.props.history.push({pathname: "/"});
        })
    }

    componentWillUnmount() {
        this.props.editStep(null)
    }

    skipTo = (step) => {
        const seconds = stringTimeToSeconds(step.startTime)
        this.player.seekTo(seconds);
        if (step.playbackRate) {
            this.setState({
                playbackRateInput: step.playbackRate,
                playbackRate: step.playbackRate
            })
        }
        this.cancelLoop();
    }

    finishEdit = (oldStep, updatedStep) =>  {
        let note = this.state.note;
        // newStep is a property on the step that signifies it was just created
        // and has not been saved yet.
        delete updatedStep.newStep;
        note.steps = note.steps.map(step => {
            return step.id === updatedStep.id ? updatedStep : step
        });
        note.update().then((new_note) => {
            this.setState({note: new_note}, () => {
                this.props.editStep(null)
            })
        })
    }

    startEdit(step) {
        this.props.editStep(step.id)
    }

    deleteStep(step) {
        console.log('state note', this.state.note, step);
        let proceed = window.confirm("Delete Step?")
        if (!proceed) {
            return
        }
        let note = this.state.note;
        note.steps = note.steps.filter(s => s.id !== step.id);
        note.update().then(updatedNote => {
            this.setState({note: updatedNote})
        });
    }

    cancelEdit = (stepToBeEditted) => {
        let note = this.state.note;
        if (stepToBeEditted.newStep) {
            note.steps = note.steps.filter(s => !s.newStep);
        } else {
            note.steps = this.state.note.steps.map(s => {
                if (s.id === stepToBeEditted.id) s.edit_mode = !s.edit_mode;
                return s
            });
        }
        this.props.editStep(null);
        this.setState({note: note})
    }

    addStep = () => {
        // creates UI for new step
        // Step not actually created until saved
        let note = this.state.note;
        let newID = this.nextStepId(note.steps);
        note.steps.push(new Step({
            newStep: true,
            id: newID
        }));
        this.setState({note: note}, () => {
            this.props.editStep(newID);
        });

    }

    ref = player => {
        this.player = player
    }

    nextStepId(steps) {
        if (!steps || steps.length === 0) return 1;
        return _.maxBy(steps, "id").id + 1;
    }

    playerReady(event) {
        console.log('player ready', event, event.target);

    }

    startEditingTitle = () => {
        this.setState({tempTitle: this.state.note.title}, () => {
            this.props.changeTitleEditMode(true);
        });
    }

    saveTitleChange = () => {
        let note = this.state.note;
        note.title = this.state.tempTitle
        note.update().then(updatedNote => {
            this.setState({note: updatedNote});
            this.props.changeTitleEditMode(false);

        })
    }


    titleChanged = (evt) => {
        this.setState({
            tempTitle: evt.target.value
        })
    }

    onProgress = ({played, playedSeconds}) => {
        if (!this.props.currentLoop) { return }
        const {startSeconds, endSeconds} = this.props.currentLoop
        if (playedSeconds < startSeconds) {
            this.player.seekTo(startSeconds)
            return
        }
        if (playedSeconds > endSeconds) {
            this.player.seekTo(startSeconds)
        }

    }

    cancelLoop = () => {
       this.props.setCurrentLoop(null)
    }

    updatePlaybackRateInput = (evt) => {
        this.setState({
            playbackRateInput: evt.target.value
        })
    }
    setPlaybackRate = () => {
        const { playbackRateInput } = this.state
        console.log(typeof playbackRateInput, isNaN(playbackRateInput))
        if (isNaN(playbackRateInput)) {
            // add other checks here
            alert('Invalid Playback Speed');
            return
        }
        this.setState({playbackRate: parseFloat(playbackRateInput)})
    }

    render() {

        if (this.state.loading) {
            return <div className="uk-container">Loading...</div>
        }

        const opts = {
            height: '390',
            width: '640',
            playerVars: { // https://developers.google.com/youtube/player_parameters
                autoplay: 1
            }
        };
        const {
            playbackRate,
            playbackRateInput,
            note,
            tempTitle,
            isOwner,
        } = this.state;
        const {
            changeTitleEditMode,
            setCurrentLoop,
            titleEditMode,
            currentLoop,
        } = this.props;
        const {steps, video_code, title, uid} = note;

        return <div className="uk-padding-large uk-padding-remove-top">
            <div data-uk-sticky>
                <YouTubePlayer ref={this.ref}
                               playing={true}
                               controls={true}
                               url={`https://www.youtube.com/watch?v=${video_code}`}
                               width={"100%"}
                               onReady={this.onReady}
                               onProgress={this.onProgress}
                               playbackRate={playbackRate}
                />
            </div>


            {titleEditMode ?
                <TitleEdit
                    tempTitle={tempTitle}
                    titleChanged={this.titleChanged}
                    saveTitleChange={this.saveTitleChange}
                    changeTitleEditMode={changeTitleEditMode}
                /> :
                <Title
                title={title} canEdit={isOwner} startEdit={this.startEditingTitle}/>
            }
            <div className="uk-flex uk-flex-row">
                <div>
                    <span>Playback speed:</span>
                    <input className="uk-input uk-form-width-small uk-margin-small-left" type="number"
                           value={playbackRateInput} name="playbackRateInput"
                           onChange={this.updatePlaybackRateInput} min="0" step="0.01"/>
                    <button className={"uk-button"} onClick={this.setPlaybackRate}>Set</button>
                </div>
                { currentLoop &&
                <button className="uk-button uk-button-default uk-flex uk-flex-middle uk-margin-left" onClick={this.cancelLoop}>
                    <span uk-icon="close"></span>
                    <span className="uk-margin-left">{`Loop ${currentLoop.startTime} : ${currentLoop.endTime}`}</span>
                </button>
                }
            </div>
            <hr className="uk-divider-icon"/>
            {/*
            // TODO add collections
            <div>
                <button data-uk-toggle="target: #add-collection-modal" type="button" className="uk-button uk-button-text">
                    Add to Collection
                </button>
                <AddToCollectionModal noteUID={uid}/>
            </div>
            <hr className="uk-divider-icon"/>
            */}
            <div>
                <ul className="uk-grid uk-child-width-1-1 uk-margin" data-uk-grid>
                    {steps.map((step, i) => step.id === this.props.editable_step ?
                        <li key={step.id}>
                            <StepEditView
                                step={step}
                                cancelEdit={this.cancelEdit}
                                finishEdit={this.finishEdit}
                            />
                        </li> :
                        <li key={step.id}>
                            <StepView step={step}
                                      skipTo={this.skipTo}
                                      isOwner={isOwner}
                                      deleteStep={this.deleteStep.bind(this, step)}
                                      startEdit={this.startEdit.bind(this, step)}
                                      setCurrentLoop={setCurrentLoop}
                            />
                        </li>
                    )}
                </ul>
            </div>
            {isOwner ?
                <div className="uk-flex uk-flex-right">
                    <button className="uk-button uk-button-primary"
                            disabled={this.props.editable_step !== null}
                            onClick={this.addStep}>Add Step</button>
                </div> : null
            }

        </div>
    }
}


const mapStateToProps = state => {
    return {
        editable_step: state.editable_step,
        titleEditMode: state.titleEditMode,
        currentLoop: state.currentLoop,

    }
};


const mapDispatchToProps = {
   editStep: editStepChange,
   changeTitleEditMode,
   setCurrentLoop
}


export default connect(mapStateToProps, mapDispatchToProps)(NotesDetailView);
