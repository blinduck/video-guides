
export const stringTimeToSeconds = (stringTime) => {

    let seconds = 0;
    let parts = stringTime.split(":").reverse();
    if (parts.length === 0) {
        return
    }
    if (parts[0]) {
        seconds = parseInt(parts[0])
    }
    if (parts[1]) {
        seconds += (60 * parseInt(parts[1]))
    }

    if (parts[2]) {
        seconds += 3600 * parseInt(parts[2])
    }
    return seconds
}

export const isTimeValid = (time) => {
    if (!time) {return true}
    let parts = time.split(":")
    if (parts.length > 3) {
        return false
    }
    for (const part of parts) {
        if (isNaN(part)) {
            return false
        }
    }
    return true
}
