import {isTimeValid} from "./timeUtils";

export { isTimeValid } from './timeUtils'

export const isStepTimeValid = (step) =>  {
    if (step.isLoop) {
        return isTimeValid(step.startTime) && isTimeValid(step.endTime)
    }
    if (step.startTime) {
        return isTimeValid(step.startTime)
    }
    return true
}
