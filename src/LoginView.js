import React from 'react';
import {DataAdapter, Store} from "./DataAdapter";
import {ReduxStore} from "./App";
import {login_state} from "./actions";

class LoginView extends React.Component {
    constructor(props) {
        super(props);
        this.formSubmitted = this.formSubmitted.bind(this);
        this.inputChanged = this.inputChanged.bind(this);
        this.props = props;
        this.state = {
            form: {
                email: "",
                password: ""
            }
        };
    }


    formSubmitted(evt) {
        evt.preventDefault();
        DataAdapter.login(this.state.form).then(resp => {
            ReduxStore.dispatch(login_state(true));
            Store.storeToken(resp.data.auth_token);
            Store.setUser(resp.data);
            this.props.history.push({pathname: "/notes"})


        }).catch(error => {
            alert(error.response.data.msg);
        });

    }

    inputChanged(evt) {
        let form = this.state.form;
        form[evt.target.name] = evt.target.value;
        this.setState({form: form});
    }


    render() {
        let {email, password} = this.state.form;
        return <div className="uk-container">
            <h3>Login</h3>
            <div className="uk-form-stacked">
                <form onSubmit={this.formSubmitted}>
                    <div className="uk-margin">
                        <label className="uk-form-label">Email Address</label>
                        <div className="uk-form-controls">
                            <input name={"email"} value={email} className={"uk-input"}
                                   type="email" onChange={this.inputChanged} required/>
                        </div>
                    </div>
                    <div className="uk-margin">
                        <label htmlFor="" className="uk-form-label">Password</label>
                        <div className="uk-form-controls">
                            <input name={"password"} value={password} className={"uk-input"}
                                   type="password" required min={"8"} onChange={this.inputChanged}/>
                        </div>
                    </div>
                    <input type="submit" value={"submit"} className={"uk-button uk-button-primary"}/>
                </form>
            </div>
        </div>
    }
}

export {LoginView}
