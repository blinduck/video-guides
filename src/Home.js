import React from "react";
import {DataAdapter} from "./DataAdapter";
import queryString from "query-string";
import {Link} from "react-router-dom";
import {PublicNoteListItem} from "./PublicNoteListItem";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.getPage = this.getPage.bind(this);
        this.updateData = this.updateData.bind(this);

        this.state = {
            loading: true,
            page: this.getPage()
        }
    }

    updateData(page) {
        DataAdapter.public_notes(page).then((resp) => {
            console.log('resp data', resp.data)
            this.setState({
                loading: false,
                notes: resp.data.results,
                page: this.getPage(),
                nextPage: resp.data.next ? page + 1 : null,
                prevPage: resp.data.previous ? page - 1 : null
            })
        }).catch(error => {
            console.log("error", error)
            alert("Unable to get your notes")
        })

    }

    getPage() {
        let params = queryString.parse(this.props.location.search);
        return parseInt(params.page) || 1
    }

    componentDidMount() {
        this.updateData(this.getPage())
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let p = this.getPage();
        if (p !== prevState.page) {
            this.updateData(p)
        }
    }


    render() {
        if (this.state.loading) {
            return <div className="uk-flex uk-flex-center">
                <div>Loading... </div>
            </div>
        }
        let {notes, prevPage, nextPage} = this.state;
        return (
            <div>
                <div className="container">
                    <h3 className="uk-text-center uk-margin-medium-top uk-margin-medium-bottom">
                        Tubenotes lets you quickly annotate videos, <br/>and loop specific sections of interest.
                    </h3>
                </div>
                <hr className="uk-divider-icon"></hr>

                <div className="uk-padding-large uk-padding-remove-top">
                    <div>
                        <h4>Latest</h4>
                    </div>

                    <div className="uk-grid uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-4@l"
                         data-uk-grid>
                        {this.state.notes.map(note => PublicNoteListItem(note))}
                    </div>
                    <div>
                        <ul className="uk-pagination uk-margin-top">
                            {prevPage ?
                                <li>
                                    <Link to={`/?page=${prevPage}`}>
                                        <span className="uk-margin-small-right"
                                              data-uk-pagination-previous></span> Previous
                                    </Link>
                                </li> : null}
                            {
                                nextPage ?
                                    <li className="uk-margin-auto-left">
                                        <Link to={`/?page=${nextPage}`}>Next
                                            <span className="uk-margin-small-left" data-uk-pagination-next></span>
                                        </Link>
                                    </li> : null
                            }
                        </ul>
                    </div>

                </div>

            </div>


        )
    }
}


export {Home}
